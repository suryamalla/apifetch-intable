 import React, { Component } from 'react'
import axios from 'axios'
 export default class App extends Component {

  state={
    data:[],
  }
  ViewPost (id,e){
    e.preventDefault();
    axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res)=>{
       alert(`ID=${id} TITLE=${res.data.title} BODY=${res.data.body}`)
       
    })
  }
   DeletePost (id,e){
    e.preventDefault();
    axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res)=>{
      alert(`element is deleted which has id ${id}  `);

      // for checking item is delete or not if item delete then title is undefine else view title
      alert(`ID=${id} ${res.data.title} this is because of deleted`) 
    })
  }
  componentDidMount() {
     
axios.get("https://jsonplaceholder.typicode.com/posts").then((res)=>{
  // let data=res.data;
  // this.setState({data});
  this.setState({data: res.data})
});

 }
   render() {
    // console.log(this.state.data)
     return (
      <>
      <table border={1}>
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Body</th>
            <th>Status</th>
            <th colSpan={2}>Action</th>
          </tr>
        </thead>
      <tbody>
        {
          this.state.data.map((item)=>{
            return(
<tr key={item.id}>
  <td>{item?.id}</td>
  <td>{item?.title}</td>
  <td>{item?.body}</td>
  <td>true</td>
  <td> <button style={{backgroundColor:'whitesmoke'}} onClick={(e)=>{ this.ViewPost(item.id,e)}} >view</button> </td>
  <td> <button style={{backgroundColor:'whitesmoke'}} onClick={(e)=>{ this.DeletePost(item.id,e)}} >Delete</button> </td>
</tr>
)
          })
        }
        </tbody>
        </table>
        {console.log(this.state.data)}
      </>
     )
   }
 }